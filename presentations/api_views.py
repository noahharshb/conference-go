import json
from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]
@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
            

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"delete": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Status not found"},
                status=400,
            )

        Presentation.objects.filter(id=id).update(**content)

        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation, 
            encoder=PresentationDetailEncoder,
            safe=False,
        )
            
    # presentations = Presentation.objects.all()
    # return JsonResponse(
    #     {"presentations": presentations},
    #     encoder=PresentationListEncoder,
    # )

    # if request.method == "GET":
    #     presentation = Presentation.objects.get(id=id)
    #     return JsonResponse(
    #         presentation,
    #         encoder=PresentationDetailEncoder,
    #         safe=False,
    #     )
    # elif request.method == "DELETE":
    #     count, _ = Presentation.objects.filter(id=id).delete()
    #     return JsonResponse({ "deleted": count > 0})
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "status" in content:
    #             status = Status.objects.get(name=content["status"])
    #             content["status"] = status
    #     except Status.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Status not found"},
    #     status=400,
    #     )
        
    #     Presentation.objects.filter(id=id).update(**content)

    #     presentation = Presentation.objects.get(id=id)
    #     return JsonResponse(
    #         presentation,
    #         encoder=PresentationDetailEncoder,
    #         safe=False,
    #     )